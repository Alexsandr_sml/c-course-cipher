using System.IO;
using System;

/**
 * Алгоритмы шифрования и дешифрования данных (Цезарь, Атбаш, Виженер и др.)
 */

class Util {
    // UTF8 коды букв
    // Английский алфавит
    public const int ENG_U_START   = (int)'a';
    public const int ENG_U_END     = (int)'z';
    public const int ENG_L_START   = (int)'A';
    public const int ENG_L_END     = (int)'Z';
    
    // Русский алфавит
    public const int RUS_U_START   = (int)'а';
    public const int RUS_U_END     = (int)'я';
    public const int RUS_L_START   = (int)'А';
    public const int RUS_L_END     = (int)'Я';

    /**
     * Проверка на допустимость UTF8 кода сивола
     * @param  {Integer}  code - UTF8 код
     * @return {Boolean}
     *
     * @private
     */
    private bool _isAvailable (int code){
        bool isAvailable = false;
        if( inRange(ENG_U_START, code, ENG_U_END) || inRange(ENG_L_START, code, ENG_L_END) ) {
            isAvailable = true;
        } else if( inRange(RUS_U_START, code, RUS_U_END) || inRange(RUS_L_START, code, RUS_L_END) ){
            isAvailable = true;
        }

        return isAvailable;
    }

    /**
     * Проверка на английский алфавит
     * @param  {Integer}  code - UTF8 код
     * @return {Boolean}
     *
     * @private
     */
    private bool _isEng (int code){
        bool isEng = false;
        if( inRange(ENG_U_START, code, ENG_U_END) || inRange(ENG_L_START, code, ENG_L_END) ) {
            isEng = true;
        }

        return isEng;
    }

    /**
     * Проверка на заглавную букву алфавит
     * @param  {Integer}  code - UTF8 код
     * @return {Boolean}
     *
     * @private
     */
    private bool _isBig (int code){
        bool isEng = _isEng( code );
        bool isBig = false;

        if( isEng ) {
            if ( inRange(ENG_U_START, code, ENG_U_END) ) {
                isBig = true;
            }
        } else {
            if ( inRange(RUS_U_START, code, RUS_U_END) ) {
                isBig = true;
            }
        }

        return isBig;
    }

    /**
     * Проверка на вхождение a2 параметра в диапазон между a1 - a3 (включительно).
     * @param  {Integer}  a1
     * @param  {Integer}  a2
     * @param  {Integer}  a3
     * @return {Boolean}
     *
     * @public
     */
    public bool inRange(int a1, int a2, int a3){
        if( a1 <= a2 && a2 <= a3 ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Смешение UTF8 кода в диапазоне алфавита.
     * @param  {Integer} code - UTF8 код
     * @param  {Integer} n    - Сила смещения
     * @return {Integer}
     *
     * @public
     */
    public int shift(int code, int n){
        int min, max, range;
        int tmpCode;

        bool isAvailable = false;
        bool isEng = false;
        bool isBig = false;

        isAvailable = _isAvailable( code );

        // Недопустимый символ
        if( !isAvailable ) { return code;};

        isEng = _isEng( code );
        isBig = _isBig( code );

        // Начало и конец
        if( isEng ) {
            if( isBig ) { 
                min = ENG_U_START;
                max = ENG_U_END;
            } else { 
                min = ENG_L_START;
                max = ENG_L_END;
            }
        } else {
            if( isBig ) { 
                min = RUS_U_START;
                max = RUS_U_END;
            } else { 
                min = RUS_L_START;
                max = RUS_L_END;
            }
        }

        range = max - min + 1;
        n %= range;

        // Смещение
        tmpCode = code + n;

        // Выравнивание
        while( tmpCode < min ) {
            tmpCode += range;
        }
        while( tmpCode > max ) {
            tmpCode -= range;
        }

        return tmpCode;
    }

    /**
     * Инвертирования UTF8 кода в диапазоне алфавита.
     * @param  {Integer} code - UTF8 код
     * @param  {Integer} n    - Сила смещения
     * @return {Integer}
     *
     * @public
     */
    public int invert(int code, int n){
        int min, max, range;
        int tmpCode;

        bool isAvailable = false;
        bool isEng = false;
        bool isBig = false;

        isAvailable = _isAvailable( code );

        // Недопустимый символ
        if( !isAvailable ) { return code;};

        isEng = _isEng( code );
        isBig = _isBig( code );

        // Начало и конец
        if( isEng ) {
            if( isBig ) { 
                min = ENG_U_START;
                max = ENG_U_END;
            } else { 
                min = ENG_L_START;
                max = ENG_L_END;
            }
        } else {
            if( isBig ) { 
                min = RUS_U_START;
                max = RUS_U_END;
            } else { 
                min = RUS_L_START;
                max = RUS_L_END;
            }
        }

        range = max - min + 1;
        n %= range;

        // Смещение
        tmpCode = code - min;
        tmpCode = max - tmpCode;
        tmpCode = tmpCode + n;

        // Выравнивание
        while( tmpCode < min ) {
            tmpCode += range;
        }
        while( tmpCode > max ) {
            tmpCode -= range;
        }

        return tmpCode;
    }

    /**
     * Получение порядкового номера UTF8 кода в диапазоне алфавита.
     * @param  {Integer} code - UTF8 код
     * @return {Integer}
     *
     * @public
     */
    public int getCharShift(int code){
        int min;
        int tmpCode;

        bool isAvailable = false;
        bool isEng = false;
        bool isBig = false;

        isAvailable = _isAvailable( code );

        // Недопустимый символ
        if( !isAvailable ) { return code;};

        isEng = _isEng( code );
        isBig = _isBig( code );

        // Начало и конец
        if( isEng ) {
            if( isBig ) { 
                min = ENG_U_START;
            } else { 
                min = ENG_L_START;
            }
        } else {
            if( isBig ) { 
                min = RUS_U_START;
            } else { 
                min = RUS_L_START;
            }
        }

        // Смещение
        tmpCode = code - min;

        return tmpCode;
    }

    /**
     * Получение порядкового номера кода начала алфавита.
     * @param  {Integer} code - UTF8 код
     * @return {Integer}
     *
     * @public
     */
    public int getAlpStart(int code){
        int min;
        int tmpCode;

        bool isAvailable = false;
        bool isEng = false;
        bool isBig = false;

        isAvailable = _isAvailable( code );

        // Недопустимый символ
        if( !isAvailable ) { return code;};

        isEng = _isEng( code );
        isBig = _isBig( code );

        // Начало и конец
        if( isEng ) {
            if( isBig ) { 
                min = ENG_U_START;
            } else { 
                min = ENG_L_START;
            }
        } else {
            if( isBig ) { 
                min = RUS_U_START;
            } else { 
                min = RUS_L_START;
            }
        }

        return min;
    }

    /**
     * Получение порядкового номера кода конца алфавита.
     * @param  {Integer} code - UTF8 код
     * @return {Integer}
     *
     * @public
     */
    public int getAlpEnd(int code){
        int max;
        int tmpCode;

        bool isAvailable = false;
        bool isEng = false;
        bool isBig = false;

        isAvailable = _isAvailable( code );

        // Недопустимый символ
        if( !isAvailable ) { return code;};

        isEng = _isEng( code );
        isBig = _isBig( code );

        // Начало и конец
        if( isEng ) {
            if( isBig ) { 
                max = ENG_U_END;
            } else { 
                max = ENG_L_END;
            }
        } else {
            if( isBig ) { 
                max = RUS_U_END;
            } else { 
                max = RUS_L_END;
            }
        }

        return max;
    }
}

class Caesar {
    private Util util = new Util();

    /**
     * Кодирование шифром
     * @param  {String}     str
     * @param  {Int}        shift
     * @return {String}
     *
     * @public
     */
    public string encode(string str, int shift) {
        int code;
        char tmpChar;

        string endStr = "";
        char[] buffer = str.ToCharArray();

        for(int i = 0, len = str.Length; i < len; i++){
            code = (int)buffer[i];

            tmpChar = Convert.ToChar( util.shift(code, shift) );
            endStr += tmpChar;
        }

        return endStr;
    }

    /**
     * Декодирование шифром
     * @param  {String}     str
     * @param  {Int}        shift
     * @return {String}
     *
     * @public
     */
    public string decode(string str, int shift) {
        int code;
        char tmpChar;

        string endStr = "";
        char[] buffer = str.ToCharArray();

        for(int i = 0, len = str.Length; i < len; i++){
            code = (int)buffer[i];

            tmpChar = Convert.ToChar( util.shift(code, -1 * shift) );
            endStr += tmpChar;
        }

        return endStr;
    }
}

class Affine {
    private Util util = new Util();

    /**
     * Кодирование шифром
     * @param  {String}     str
     * @return {String}
     *
     * @public
     */
    public string encode(string str) {
        int code;
        char tmpChar;
        int shift;
        int startCode;

        string endStr = "";
        char[] buffer = str.ToCharArray();

        for(int i = 0, len = str.Length; i < len; i++){
            code = (int)buffer[i];

            startCode = util.getAlpStart( code );

            shift = util.getCharShift( code );
            shift = 3 * shift + 4;

            tmpChar = Convert.ToChar( util.shift(startCode, shift) );
            endStr += tmpChar;
        }

        return endStr;
    }

    /**
     * Декодирование шифром
     * @param  {String}     str
     * @return {String}
     *
     * @public
     */
    public string decode(string str) {
        int code;
        char tmpChar;
        int shift;
        int startCode;

        string endStr = "";
        char[] buffer = str.ToCharArray();

        for(int i = 0, len = str.Length; i < len; i++){
            code = (int)buffer[i];

            startCode = util.getAlpStart( code );

            shift = util.getCharShift( code );
            shift = (shift - 4) * 9;

            tmpChar = Convert.ToChar( util.shift(startCode, shift) );
            endStr += tmpChar;
        }

        return endStr;
    }
}

class Atbash {
    private Util util = new Util();

    /**
     * Кодирование шифром
     * @param  {String}     str
     * @return {String}
     *
     * @public
     */
    public string encode(string str) {
        int code;
        char tmpChar;

        string endStr = "";
        char[] buffer = str.ToCharArray();

        for(int i = 0, len = str.Length; i < len; i++){
            code = (int)buffer[i];

            tmpChar = Convert.ToChar( util.invert(code, 0) );
            endStr += tmpChar;
        }

        return endStr;
    }

    /**
     * Декодирование шифром
     * @param  {String}     str
     * @return {String}
     *
     * @public
     */
    public string decode(string str) {
        int code;
        char tmpChar;

        string endStr = "";
        char[] buffer = str.ToCharArray();

        for(int i = 0, len = str.Length; i < len; i++){
            code = (int)buffer[i];

            tmpChar = Convert.ToChar( util.invert(code, 0) );
            endStr += tmpChar;
        }

        return endStr;
    }
}

class Vigenere {
    private Util util = new Util();

    /**
     * Получение полного ключа вижинера
     * @param  {String} str
     * @param  {String} key
     * @return {String}
     *
     * @private
     */
    private string getFullKey(string str, string key) {
        string tmpKey   = "";

        char[] buffer   = key.ToCharArray();
        int bufferLen   = key.Length; // Длина ключа
        int len         = str.Length; // Длина кода

        int counter = 0;
        while(counter < len){
            tmpKey += buffer[ (counter % bufferLen) ];
            counter++;
        }

        return tmpKey;
    }

    /**
     * Кодирование шифром
     * @param  {String}     str
     * @param  {String}     key
     * @return {String}
     *
     * @public
     */
    public string encode(string str, string key) {
        int codeStr, codeKey, shift;
        char tmpChar;
        
        string fullKey = getFullKey(str, key);

        string endStr = "";
        char[] bufferStr = str.ToCharArray();
        char[] bufferKey = fullKey.ToCharArray();

        for(int i = 0, len = str.Length; i < len; i++){
            codeStr = (int)bufferStr[i];
            codeKey = (int)bufferKey[i];

            shift = util.getCharShift( codeKey );

            tmpChar = Convert.ToChar( util.shift(codeStr, shift) );
            endStr += tmpChar;
        }

        return endStr;
    }

    /**
     * Декодирование шифром
     * @param  {String}     str
     * @param  {String}     key
     * @return {String}
     *
     * @public
     */
    public string decode(string str, string key) {
        int codeStr, codeKey, shift;
        char tmpChar;
        
        string fullKey = getFullKey(str, key);

        string endStr = "";
        char[] bufferStr = str.ToCharArray();
        char[] bufferKey = fullKey.ToCharArray();

        for(int i = 0, len = str.Length; i < len; i++){
            codeStr = (int)bufferStr[i];
            codeKey = (int)bufferKey[i];

            shift = util.getCharShift( codeKey );

            tmpChar = Convert.ToChar( util.shift(codeStr, -1 * shift) );
            endStr += tmpChar;
        }

        return endStr;
    }
}

class Program {
    static void Main() {
        // Debug
        string text;
        string detext;
        string endText;

        Console.WriteLine("================================================================================");
        Console.WriteLine("Алгоритмы шифрования и дешифрования данных (Цезарь, Атбаш, Виженер и др.)");
        Console.WriteLine("================================================================================");

        
        // Цезарь
        Console.WriteLine("--------------------------------------------------------------------------------");
        Console.WriteLine("Шифр цезаря");
        Console.WriteLine("--------------------------------------------------------------------------------");

        /**
         * @example
         * Hello, Caesar!
         * ->
         * Khoor, Fdhvdu!
         */
        Console.WriteLine("Введите текст для кодирования: ");
        text = Console.ReadLine();

        Caesar caesar = new Caesar();
        endText = caesar.encode(text, 3);
        Console.WriteLine("Зашифрованный текст: " + endText);

        Console.WriteLine("Введите текст для декодирования: ");
        detext = Console.ReadLine();

        endText = caesar.decode(detext, 3);
        Console.WriteLine("Декодированный текст: " + endText);

        Console.WriteLine("\n\n\n");

        // Аффинный шифр
        Console.WriteLine("--------------------------------------------------------------------------------");
        Console.WriteLine("Аффинный шифр");
        Console.WriteLine("--------------------------------------------------------------------------------");

        /**
         * @example
         * Hello, Affine!
         * ->
         * Zqllu, Ettcrq!
         */
        Console.WriteLine("Введите текст для кодирования: ");
        text = Console.ReadLine();

        Affine affine = new Affine();
        endText = affine.encode(text);
        Console.WriteLine("Зашифрованный текст: " + endText);

        Console.WriteLine("Введите текст для декодирования: ");
        detext = Console.ReadLine();

        endText = affine.decode(detext);
        Console.WriteLine("Декодированный текст: " + endText);

        Console.WriteLine("\n\n\n");
        
        // Атбаш
        Console.WriteLine("--------------------------------------------------------------------------------");
        Console.WriteLine("Шифр атбаш");
        Console.WriteLine("--------------------------------------------------------------------------------");

        /**
         * @example
         * Hello, Atbash!
         * ->
         * Svool, Zgyzhs!
         */
        Console.WriteLine("Введите текст для кодирования: ");
        text = Console.ReadLine();

        Atbash atbash = new Atbash();
        endText = atbash.encode(text);
        Console.WriteLine("Зашифрованный текст: " + endText);

        Console.WriteLine("Введите текст для декодирования: ");
        detext = Console.ReadLine();

        endText = atbash.decode(detext);
        Console.WriteLine("Декодированный текст: " + endText);

        Console.WriteLine("\n\n\n");
        
        // Виженер
        Console.WriteLine("--------------------------------------------------------------------------------");
        Console.WriteLine("Шифр Виженера");
        Console.WriteLine("--------------------------------------------------------------------------------");

        /**
         * @example
         * text:    Hello, Vigenere!
         * key:     LEMON
         * ->
         * Sixzb, Hwtprqfr!
         */
        string vigenereKey;

        Console.WriteLine("Введите текст для кодирования: ");
        text = Console.ReadLine();

        Console.WriteLine("Введите ключ для кодирования: ");
        vigenereKey = Console.ReadLine();

        Vigenere vigenere = new Vigenere();
        endText = vigenere.encode(text, vigenereKey);
        Console.WriteLine("Зашифрованный текст: " + endText);

        Console.WriteLine("Введите текст для декодирования: ");
        detext = Console.ReadLine();

        Console.WriteLine("Введите ключ для декодирования: ");
        vigenereKey = Console.ReadLine();

        endText = vigenere.decode(detext, vigenereKey);
        Console.WriteLine("Декодированный текст: " + endText);
    }
}
