# Курсовая работа. 2 курс
Алгоритмы шифрования и дешифрования данных (Цезарь, Атбаш, Виженер и др.)

--------------------------------------------------------------------------------

## Примечание
1. Каждый индекс буквы в алфавите взят с учетом программирования (Позиция в алфавите -1), т.е. `А` это [**0**], а не **1** и т.д. 
2. Смещаются и шифруются только буквы **латиницы** и **кириллицы**. Т.е. пробелы, точки и прочее остается неизменным.
3. Я не включаю в кирилицу букву `Ё`, так что в алфавите остается только `32` буквы

--------------------------------------------------------------------------------

## Алгоритмы
### Шифр Цезаря
**Ссылка**: [wikipedia](https://ru.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F)

Смысл шифра в смещение буквы алфавита на `+3` позиции. Т.е:
* `А` -> `Г`
* `З` -> `К`
* и т.д.

#### Шифрование
Каждая буква смещается на `+3` по алфавите. Если позиция вызодит за предел алфавите, то делится на кол-во букв в алфавите по модулю.

```sh
Допустим Я [31]
31 + 3 = 34
34 % 32 = 2

[2] = В
```

#### Дешифровка
Вточности так же как и шифрование, только каждая буква зашифрованного текста смещается на `-3`. Если позиция вызодит за `0`, тогда прибавляем кол-во букв в алфавите.

```sh
Допустим Б [1]
1 - 3 = -2
-2 + 32 = 30 # Это тоже самое, что и -2 %% 32
[30] = Ю
```

--------------------------------------------------------------------------------

### Аффинный шифр
**Ссылка**: [wikipedia](https://ru.wikipedia.org/wiki/%D0%90%D1%84%D1%84%D0%B8%D0%BD%D0%BD%D1%8B%D0%B9_%D1%88%D0%B8%D1%84%D1%80)

Шифр схож с шифром Цезаря, только смещение происходит по формуле `3 * x + 4`, где `x` - позиция буквы алфавита.


#### Шифрование
Каждая буква заменяется на букву на позиции `3 * x + 4`, где `x` - позиция буквы алфавита. Если позиция вызодит за предел алфавите, то делится на кол-во букв в алфавите по модулю.

```sh
Допустим A [0]
3 * 0 + 4 = 4
4 % 26 = 4

[4] = E

Если это буква Q [16]
3 * 16 + 4 = 52
52 % 26 = 0

[0] = A
```

#### Дешифровка
Каждая буква шифра заменяется на букву на позиции `9 * (y - 4) % кол-во букв в алфавите`, где `y` - позиция буквы алфавита. Если позиция вызодит за `0`, тогда прибавляем кол-во букв в алфавите, до тех пор пока не станет положительной.

```sh
Допустим K [10]
9 * (10 - 4) = 54
54 % 26 = 2

[2] = C

Если это буква R [17]
9 * (17 - 4) = 117
117 % 26 = 13

[13] = N

Если это буква A [0]
9 * (0 - 4) = -36
-36 + 26 + 26 = 16
16 % 26 = 16

[16] = Q
```

--------------------------------------------------------------------------------

### Атбаш
**Ссылка**: [wikipedia](https://ru.wikipedia.org/wiki/%D0%90%D1%82%D0%B1%D0%B0%D1%88)

Смысл шифра в сопоставлении букве шифруемого слова, букву с конца алфавита. Т.е:
* `А` -> `Я`
* `Б` -> `Ю`
* и т.д.

#### Шифрование
Каждая буква заменяется на `кол-во букв в алфавите - 1 - позиция буквы`.

```sh
Допустим буква А [0]
31 - 0 = 31

[31] = Я

Если это буква В [2]
31 - 2 = 29

[29] = Ю

Если это буква Ч [23]
31 - 23 = 8

[8] = И

и т.д.
```

#### Дешифровка
Ничем не отличается от шифрования.

--------------------------------------------------------------------------------

### Шифр Виженера
**Ссылка**: [wikipedia](https://ru.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%92%D0%B8%D0%B6%D0%B5%D0%BD%D0%B5%D1%80%D0%B0)

Шифр подразумевает под собой наличие **кодируемого слова** и **ключа**. И последующее смещение каждой буквы кодируемого слова на позицию соответствующей по позиции буквы в алфавите.

#### Шифрование
Допустим у нас есть кодируемое слово: `ATTACKATDAWN`; и ключ `LEMON`. Так как ключ короче кодируемого слова, его нужно дополнить им же, пока он не станет равен длине кодируемого слова.

* ATTACKATDAWN
* LEMON**LEMONLE**

Т.е. мы дополнили ключ словом `LEMONLE`, получив полный ключ `LEMONLEMONLE`
Теперь мы должны сместить каждую букву в кодируемом слову на позицию буквы в алфавите ключа.

```sh
Допустим первая буква A [0]
должна сместиться на L [11]
0 + 11 = 11

[11] = L

Следующая буква T [19]
должна сместиться на E [4]
19 + 4 = 23

[23] = X

И т.д.
```

Обойдя так каждую цифру, мы получим зашифрованное слово `LXFOPVEFRNHR`

#### Дешифровка
Как и в случае с шифром цезаря, дешифровка происходит в обратноп порядке.
Сначала мы составляем полный ключ, в нашем случае это `LEMONLEMONLE`, и смещаем каждую букву зашифрованного слова на позицию буквы в алфавите ключа умноженную на `-1`.

```sh
Допустим первая буква зашифрованного текста L [11]
она доджна сместиться на L [11]
11 - 11 = 0

[0] = A

Следующая буква зашифрованного текста X [23]
должна сместиться на E [4]
23 - 4 = 19

[19] = T

И т.д.
```

В конечном итоге мы получим исходное слово `ATTACKATDAWN`
